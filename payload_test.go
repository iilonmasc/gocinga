package main

import (
	"fmt"
	"gitlab.com/sambadevi/gocinga/v2/icinga"
	"gitlab.com/sambadevi/sambalib/v3/logutil"
	"gitlab.com/sambadevi/sambalib/v3/sysutil"
	"testing"
	"time"
)

func TestHostPayload(t *testing.T) {
	fn := "[ " + sysutil.FuncName() + " ]:"
	builder := &icinga.PayloadBuilder{}
	builder.Init()
	testLogger := &logutil.Logger{}
	testLogger.SetOptions(logutil.LogLevel(logutil.All), logutil.Prefix("testing"))
	type hostPayload struct {
		Server string
		Vars   []string
	}
	cases := []struct {
		description string
		in          hostPayload
		want        string
	}{
		{"generates empty payload", hostPayload{Server: "", Vars: []string{}}, `{"attrs":{"address":"","check_command":"http"}}`},
		{"generates payload with vars.os", hostPayload{Server: "example.com", Vars: []string{"os=Linux"}}, `{"attrs":{"address":"example.com","check_command":"http","vars.os":"Linux"}}`},
		{"generates payload with vars.os and vars.uri", hostPayload{Server: "example.com", Vars: []string{"os=Linux", "uri=/somewhere"}}, `{"attrs":{"address":"example.com","check_command":"http","vars.os":"Linux","vars.uri":"/somewhere"}}`},
		{"generates payload with custom fields", hostPayload{Server: "example.com", Vars: []string{"os=Linux", "uri=/somewhere", "custom=custom_value", "custom2=even_more_values"}}, `{"attrs":{"address":"example.com","check_command":"http","vars.os":"Linux","vars.uri":"/somewhere","vars.custom":"custom_value","vars.custom2":"even_more_values"}}`},
	}
	for _, c := range cases {
		got, err := builder.CreateHostPayload(c.in.Server, c.in.Vars)
		testLogger.CheckCritical(err, 1)
		if got != c.want {
			t.Errorf("%q %q icinga.CreateHostPayload(%q) == %q, want %q", fn, c.description, c.in, got, c.want)
		} else {
			fmt.Println(fn, c.description, ": ✓")
		}
	}
}
func TestDowntimePayload(t *testing.T) {
	fn := "[ " + sysutil.FuncName() + " ]:"
	builder := &icinga.PayloadBuilder{}
	builder.Init()
	testLogger := &logutil.Logger{}
	testLogger.SetOptions(logutil.LogLevel(logutil.Critical), logutil.Prefix("testing"))
	zone, _ := time.Now().Zone()
	start, _ := time.Parse(time.RFC822, "01 Dec 34 12:14"+" "+zone)
	duration, _ := time.ParseDuration("20m")
	end, _ := time.Parse(time.RFC822, "01 Dec 34 12:34"+" "+zone)
	type downtimePayload struct {
		Message  string
		Author   string
		Downtime icinga.Downtime
	}
	cases := []struct {
		description string
		in          downtimePayload
		want        string
	}{
		{
			"generates regular payload",
			downtimePayload{Message: "World Domination", Author: "Professor Chaos", Downtime: icinga.Downtime{Begin: start, Duration: duration, End: end}},
			`{"start_time":2048580840,"end_time":2048582040,"duration":1200,"author":"Professor Chaos","comment":"World Domination"}`,
		},
	}
	for _, c := range cases {
		got, err := builder.CreateDowntimePayload(c.in.Message, c.in.Author, c.in.Downtime)
		testLogger.CheckCritical(err, 1)
		if got != c.want {
			t.Errorf("%q %q icinga.CreateDowntimePayload(%q) == %q, want %q", fn, c.description, c.in, got, c.want)
		} else {
			fmt.Println(fn, c.description, ": ✓")
		}
	}
}
func TestValidation(t *testing.T) {
	fn := "[ " + sysutil.FuncName() + " ]:"
	builder := &icinga.PayloadBuilder{}
	builder.Init()
	testLogger := &logutil.Logger{}
	testLogger.SetOptions(logutil.LogLevel(logutil.Critical), logutil.Prefix("testing"))
	cases := []struct {
		description string
		in          string
		want        string
	}{
		{"invalid unclosed json returns error", "{!", "invalid character '!' looking for beginning of object key string"},
		{"json missing commas returns error", `{"something": "yes" "another": "also}`, "invalid character '\"' after object key:value pair"},
		{"empty json returns error", "", "unexpected end of JSON input"},
		{"valid json returns nil", `{"attrs":{"address":"example.com","check_command":"http","vars.os":"Linux"}}`, ""},
	}
	var returnValue = ""
	for _, c := range cases {
		got := builder.ValidatePayload(c.in)
		if got == nil {
			returnValue = ""
		} else {
			returnValue = got.Error()
		}
		if returnValue != c.want {
			t.Errorf("%q %q icinga.ValidatePayload(%q) == %q, want %q", fn, c.description, c.in, got, c.want)
		} else {
			fmt.Println(fn, c.description, ": ✓")
		}
	}
}
