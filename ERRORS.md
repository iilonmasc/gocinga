|Exit Code| Meaning|
|---|---|
|0| Everything is okay|
|1| Generic error|
|2| Parameter mismatch|
|3| Misconfiguration or configuration missing|
|4|Payload Error while reading or validating the payload data|
|5|Error while authenticating against configured api|
|6|Icinga API Error|