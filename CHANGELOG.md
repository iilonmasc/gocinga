# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [3.1.0](https://gitlab.com/sambadevi/gocinga/compare/v3.0.2...v3.1.0) (2019-09-19)


### Bug Fixes

* remove os.Exit from api calls ([1dce636](https://gitlab.com/sambadevi/gocinga/commit/1dce636))


### Features

* add `--pretty` flag for formatted json output ([a0d549d](https://gitlab.com/sambadevi/gocinga/commit/a0d549d))



### [3.0.2](https://gitlab.com/sambadevi/gocinga/compare/v3.0.1...v3.0.2) (2019-07-19)


### Bug Fixes

* **payload.downtime:** PayloadBuilder.BuildDowntime will now exit with exit code 4 (ExitPayload) if ([6351be0](https://gitlab.com/sambadevi/gocinga/commit/6351be0)), closes [#14](https://gitlab.com/sambadevi/gocinga/issues/14)



### [3.0.1](https://gitlab.com/sambadevi/gocinga/compare/v3.0.0...v3.0.1) (2019-07-19)


### Bug Fixes

* add http status code checking to execute function ([36dffe9](https://gitlab.com/sambadevi/gocinga/commit/36dffe9)), closes [#6](https://gitlab.com/sambadevi/gocinga/issues/6)



## [3.0.0](https://gitlab.com/sambadevi/gocinga/compare/v2.0.2...v3.0.0) (2019-07-18)


### Bug Fixes

* add check for empty payload on DELETE `--dry-run` ([a0bd90a](https://gitlab.com/sambadevi/gocinga/commit/a0bd90a))


### chore

* update go module path ([18f73c5](https://gitlab.com/sambadevi/gocinga/commit/18f73c5)), closes [#13](https://gitlab.com/sambadevi/gocinga/issues/13) [#7](https://gitlab.com/sambadevi/gocinga/issues/7) [#8](https://gitlab.com/sambadevi/gocinga/issues/8) [#9](https://gitlab.com/sambadevi/gocinga/issues/9)


### Features

* update package to completely use sambalib/v3 ([499fb8a](https://gitlab.com/sambadevi/gocinga/commit/499fb8a))
* **icinga.api:** add logging from sambalib/v3 ([15eda89](https://gitlab.com/sambadevi/gocinga/commit/15eda89))
* **icinga.Configurator:** update configuration code ([5d3e9f8](https://gitlab.com/sambadevi/gocinga/commit/5d3e9f8)), closes [#9](https://gitlab.com/sambadevi/gocinga/issues/9)
* **icinga.payload:** introducing the PayloadBuilder ([96b3725](https://gitlab.com/sambadevi/gocinga/commit/96b3725))
* add `--version` flag ([6f15391](https://gitlab.com/sambadevi/gocinga/commit/6f15391))
* add passcmd field to configuration ([9d4fce2](https://gitlab.com/sambadevi/gocinga/commit/9d4fce2)), closes [#8](https://gitlab.com/sambadevi/gocinga/issues/8)
* add rudimentary stdin support with `--raw -` ([ff061d7](https://gitlab.com/sambadevi/gocinga/commit/ff061d7))


### BREAKING CHANGES

* You now have to use `GO111MODULE=on go get gitlab.com/sambadevi/gocinga/v3` to
install the newest version of gocinga
* configuration has been renamed to .gocinga.yml and the configuration does not allow
the `access` field anymore, instead you can specify username and password directly



### [2.0.2](https://gitlab.com/sambadevi/gocinga/compare/v2.0.1...v2.0.2) (2019-07-02)


## [2.0.1](https://gitlab.com/sambadevi/gocinga/compare/v2.0.0...v2.0.1) (2019-06-30)

### Build System

* add go.mod ([871e165](https://gitlab.com/sambadevi/gocinga/commit/871e165))


## [2.0.0](https://gitlab.com/sambadevi/gocinga/compare/v1.3.0...v2.0.0) (2019-06-30)

### Features

* add --raw and -r flags for raw json input ([ae03dc0](https://gitlab.com/sambadevi/gocinga/commit/ae03dc0)), closes [#1](https://gitlab.com/sambadevi/gocinga/issues/1)
* add --vars flag ([588a622](https://gitlab.com/sambadevi/gocinga/commit/588a622)), closes [#3](https://gitlab.com/sambadevi/gocinga/issues/3)


### BREAKING CHANGES

* --path and --role flags are removed in favour of --vars

## [1.3.0](https://gitlab.com/sambadevi/gocinga/compare/v1.2.0...v1.3.0) (2019-05-28)

### Features

* **parameters:** tell the user if a parameter is missing ([a28e43f](https://gitlab.com/sambadevi/gocinga/commit/a28e43f)), closes [#4](https://gitlab.com/sambadevi/gocinga/issues/4)
* accept base64 passwords only ([de82c66](https://gitlab.com/sambadevi/gocinga/commit/de82c66)), closes [#5](https://gitlab.com/sambadevi/gocinga/issues/5)
* accept base64 passwords only ([4f0e63d](https://gitlab.com/sambadevi/gocinga/commit/4f0e63d))


# [1.2.0](https://gitlab.com/sambadevi/gocinga/compare/v1.1.4...v1.2.0) (2019-05-14)

### Bug Fixes

* add v1/ to endpoint const ([c0c23b2](https://gitlab.com/sambadevi/gocinga/commit/c0c23b2))
* remove config.Host from createPayload ([1e00df6](https://gitlab.com/sambadevi/gocinga/commit/1e00df6))
* remove trailing slash from api url ([07d1463](https://gitlab.com/sambadevi/gocinga/commit/07d1463))

### Features

* add --dry-run ([a5ba71d](https://gitlab.com/sambadevi/gocinga/commit/a5ba71d))
* add --save-config flag to create ~/.icinga-config.yml ([47173e8](https://gitlab.com/sambadevi/gocinga/commit/47173e8))
* add downtime functionality ([62e19f2](https://gitlab.com/sambadevi/gocinga/commit/62e19f2))
* add downtime functions and author information ([e6286d1](https://gitlab.com/sambadevi/gocinga/commit/e6286d1))
* add downtime payload generation ([49e0b46](https://gitlab.com/sambadevi/gocinga/commit/49e0b46))
* add type flag ([c990aec](https://gitlab.com/sambadevi/gocinga/commit/c990aec))

## [1.1.4](https://gitlab.com/sambadevi/gocinga/compare/v1.1.3...v1.1.4) (2019-04-05)

### Code refactoring

* rename ApiEndpoint to HostsEndpoint
* **structure:** exclude logging and error handling functions to separate libraries

# [1.1.3](https://gitlab.com/sambadevi/gocinga/compare/v1.1.2...v1.1.3) (2019-03-19)

### Bug Fixes

* switch '=' with ':' in createPayload to create valid json ([019f2b2](https://gitlab.com/sambadevi/gocinga/commit/019f2b2))


## [1.1.2](https://gitlab.com/sambadevi/gocinga/compare/v1.1.1...v1.1.2) (2019-03-19)


### Bug Fixes

* update yaml field configuration ([5d4c53d](https://gitlab.com/sambadevi/gocinga/commit/5d4c53d))


## [1.1.1](https://gitlab.com/sambadevi/gocinga/compare/v1.1.0...v1.1.1) (2019-03-19)


### Bug Fixes

* avoid calling array indices on empty array ([9e3bbb5](https://gitlab.com/sambadevi/gocinga/commit/9e3bbb5))


# [](https://gitlab.com/sambadevi/gocinga/compare/v1.0.0...v1.1.0) (2019-03-19)


### Features

* add `--show-config/-c` option ([17a5170](https://gitlab.com/sambadevi/gocinga/commit/17a5170))
* enhance config generation ([08fb32d](https://gitlab.com/sambadevi/gocinga/commit/08fb32d))


##  (2019-03-18)

### Features

* init ([d77d714](https://gitlab.com/sambadevi/gocinga/commit/d77d714))
