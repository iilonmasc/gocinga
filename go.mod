module gitlab.com/sambadevi/gocinga/v3

go 1.12

require (
	github.com/sambadevi/getopt/v2 v2.0.1
	gitlab.com/sambadevi/gocinga/v2 v2.0.2
	gitlab.com/sambadevi/sambalib/v2 v2.0.2
	gitlab.com/sambadevi/sambalib/v3 v3.1.1
	gopkg.in/yaml.v2 v2.2.2
)
