package icinga

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	"gitlab.com/sambadevi/sambalib/v3/logutil"
	"gitlab.com/sambadevi/sambalib/v3/stringutil"
)

const DowntimeEndpoint string = "/v1/objects/downtimes"
const HostEndpoint string = "/v1/objects/hosts"
const ServiceEndpoint string = "/v1/objects/services"

const ScheduleDowntimeEndpoint string = "/v1/actions/schedule-downtime"
const RemoveDowntimeEndpoint string = "/v1/actions/remove-downtime"

func (api *Api) Init(url string) {
	api.Url = url
	api.Endpoints.Hosts = Endpoint{Name: "hosts", Path: HostEndpoint}
	api.Endpoints.Services = Endpoint{Name: "services", Path: ServiceEndpoint}
	api.Endpoints.Downtimes = Endpoint{Name: "downtimes", Path: DowntimeEndpoint}
	api.Endpoints.DowntimeSchedule = Endpoint{Name: "schedule-downtime", Path: ScheduleDowntimeEndpoint}
	api.Endpoints.DowntimeRemove = Endpoint{Name: "remove-downtime", Path: RemoveDowntimeEndpoint}
	api.SkipTLS = false
	api.DryRun = false
	api.Logger = &logutil.Logger{}
	api.Logger.SetOptions(logutil.LogLevel(logutil.Critical), logutil.Prefix("api"))

}
func (api *Api) DisableTLS() {
	api.SkipTLS = true
}
func (api *Api) EnableDryRun() {
	api.DryRun = true
}
func (api *Api) EnableVerbosity() {
	api.Logger.SetOptions(logutil.LogLevel(logutil.All))
}
func (api *Api) EnablePrettify() {
	api.Prettify = true
}
func (api *Api) SetAccess(username, password string) {
	pass, err := stringutil.StrFromBase64(password)
	api.Logger.CheckCritical(err, ExitConfig)
	api.Auth.User = username
	api.Auth.Pass = pass
}

func (api *Api) CreateClient() {
	client := &http.Client{}
	if api.SkipTLS {
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		client.Transport = tr
	}
	api.HTTPClient = client
}

func (api *Api) CreateRequest(command, url, payload string) *http.Request {
	req, err := http.NewRequest(command, url, strings.NewReader(payload))
	api.Logger.CheckCritical(err, ExitError)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")

	if api.DryRun && payload != "" {
		formattedPayload := &bytes.Buffer{}
		if err := json.Indent(formattedPayload, []byte(payload), "", "  "); err != nil {
			api.Logger.CheckWarning(err)
		}
		fmt.Printf("payload: \n%s\n", formattedPayload.String())
	}
	req.SetBasicAuth(api.Auth.User, api.Auth.Pass)
	api.Logger.Info("sending:", command, url)
	api.Logger.Info("payload:", payload)
	return req
}
func (api *Api) Execute(req *http.Request) *http.Response {
	if api.DryRun {
		os.Exit(ExitOkay)
	}
	resp, err := api.HTTPClient.Do(req)
	api.Logger.CheckCritical(err, ExitError)
	defer resp.Body.Close()
	responseData, err := ioutil.ReadAll(resp.Body)
	api.Logger.CheckCritical(err, ExitError)
	fmt.Println(string(responseData))
	return resp
}

func (api *Api) ResolveStatus(resp *http.Response) {
	switch resp.StatusCode {
	case http.StatusOK:
		os.Exit(ExitOkay)
	case http.StatusForbidden, http.StatusUnauthorized:
		api.Logger.Critical("authentication failure, server responded", resp.Status)
		os.Exit(ExitAuth)
	case http.StatusBadGateway, http.StatusInternalServerError, http.StatusServiceUnavailable:
		api.Logger.Critical("server failure, server responded", resp.Status)
		os.Exit(ExitServer)
	default:
		api.Logger.Warning("unhandled http code, server responded", resp.Status)
		os.Exit(ExitOkay)
	}
}

func (api *Api) Add(object, payload string, endpoint Endpoint) {
	var url string
	var resp *http.Response
	command := http.MethodPut
	if endpoint.Name == "downtimes" {
		command = http.MethodPost
		endpoint = api.Endpoints.DowntimeSchedule
		url = api.Url + endpoint.Path
		hostUrl := url + "?type=Host&filter=host.name==%22" + object + "%22"
		serviceUrl := url + "?type=Service&filter=host.name==%22" + object + "%22"
		if api.Prettify {
			hostUrl = hostUrl + "&pretty=1"
			serviceUrl = serviceUrl + "&pretty=1"
		}
		req := api.CreateRequest(command, hostUrl, payload)
		resp = api.Execute(req)
		req = api.CreateRequest(command, serviceUrl, payload)
		resp = api.Execute(req)
	} else {
		url = api.Url + endpoint.Path + "/" + object
		if api.Prettify {
			url = url + "?pretty=1"
		}
		req := api.CreateRequest(command, url, payload)
		resp = api.Execute(req)
	}
	api.ResolveStatus(resp)
}
func (api *Api) List(endpoint Endpoint) {
	var url, payload string
	var resp *http.Response
	command := http.MethodGet
	url = api.Url + endpoint.Path
	if api.Prettify {
		url = url + "?pretty=1"
	}
	req := api.CreateRequest(command, url, payload)
	resp = api.Execute(req)
	api.ResolveStatus(resp)

}
func (api *Api) Delete(object, payload string, endpoint Endpoint) {
	var url string
	var resp *http.Response
	command := http.MethodDelete
	if endpoint.Name == "downtimes" {
		command = http.MethodPost
		endpoint = api.Endpoints.DowntimeRemove
		url = api.Url + endpoint.Path
		hostUrl := url + "?type=Host&filter=host.name==%22" + object + "%22"
		serviceUrl := url + "?type=Service&filter=host.name==%22" + object + "%22"
		if api.Prettify {
			hostUrl = hostUrl + "&pretty=1"
			serviceUrl = serviceUrl + "&pretty=1"
		}
		req := api.CreateRequest(command, hostUrl, payload)
		resp = api.Execute(req)
		req = api.CreateRequest(command, serviceUrl, payload)
		resp = api.Execute(req)
	} else {
		url = api.Url + endpoint.Path + "/"

		object += "?cascade=1"
		url += object
		if api.Prettify {
			url = url + "&pretty=1"
		}
		req := api.CreateRequest(command, url, payload)
		resp = api.Execute(req)
	}
	api.ResolveStatus(resp)
}
