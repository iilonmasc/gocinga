package icinga

import (
	"bytes"
	"gitlab.com/sambadevi/sambalib/v3/logutil"
	"gitlab.com/sambadevi/sambalib/v3/stringutil"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
	"os/exec"
	"os/user"
	"strings"
	"text/template"
)

func (configurator *Configurator) Init() {
	configurator.Logger = &logutil.Logger{}
	configurator.Logger.SetOptions(logutil.LogLevel(logutil.Critical), logutil.Prefix("config"))
}

func (configurator *Configurator) EnableVerbosity() {
	configurator.Logger.SetOptions(logutil.LogLevel(logutil.All))
}

func (configurator *Configurator) ShowConfig() {
	tmpl, _ := template.New("config").Parse(`
  Access-Token:     {{ .Access }}
  Icinga-User:      {{ .User }}
  Downtime-Author:  {{ .Author }}
  Icinga-Password:  {{ .Password }}
  Icinga-API:       {{ .Api }}
  Icinga-Host:      {{ .Host }}
  {{- if .Port}}{{.Port}}{{ end }}
  `)
	_ = tmpl.Execute(os.Stdout, configurator.Config)
	os.Exit(ExitOkay)
}

func (configurator *Configurator) SaveConfig() {
	usr, err := user.Current()
	configurator.Logger.CheckWarning(err)
	configFilePath := usr.HomeDir + "/.gocinga.yml"
	configFile, err := os.Create(configFilePath)
	configurator.Logger.CheckWarning(err)
	defer configFile.Close()
	payloadString := `# config file created with gocinga --save-config
{{ if .User }}user: "{{ .User }}"{{ end }}
{{ if .Password }}password: "{{ .Password}}"{{ end }}
{{ if .Api }}api: {{ .Api }}{{ end }}
{{ if .Author }}author: {{ .Author }}{{ end }}`
	tmpl, err := template.New("config").Parse(payloadString)
	configurator.Logger.CheckWarning(err)
	buffer := new(bytes.Buffer)
	err = tmpl.Execute(buffer, configurator.Config)
	configurator.Logger.CheckCritical(err, ExitConfig)
	_, err = configFile.WriteString(buffer.String())
	configurator.Logger.CheckCritical(err, ExitConfig)
	err = configFile.Sync()
	configurator.Logger.CheckCritical(err, ExitConfig)
	configurator.Logger.Info("saved configuration to", configFilePath)
	os.Exit(ExitOkay)

}
func (configurator *Configurator) BuildConfig(apiFlag, userFlag, passwordFlag, authorFlag, fileFlag string) {
	// Gather configurations from environment, user config and parameters
	var env, conf Configuration
	endConfiguration := Configuration{}
	if fileFlag != "" {
		endConfiguration = configurator.BuildFromFile(fileFlag)
	} else {
		env = configurator.GetEnvVariables()
		conf = configurator.GetConfVariables()
		endConfiguration = configurator.GetApi(apiFlag, env, conf, endConfiguration)
		endConfiguration = configurator.GetAuthor(authorFlag, endConfiguration, env, conf)
		endConfiguration = configurator.GetUser(userFlag, endConfiguration, env, conf, passwordFlag)
	}
	// set api
	endConfiguration = configurator.GetHost(endConfiguration)
	endConfiguration = configurator.RunPassCMD(endConfiguration)
	endConfiguration.Access = endConfiguration.User + ":" + endConfiguration.Password
	configurator.Config = endConfiguration
}

func (configurator *Configurator) GetHost(endConfiguration Configuration) Configuration {
	endConfiguration.Api = strings.TrimRight(endConfiguration.Api, "/")
	if apiUrl := strings.Split(endConfiguration.Api, "/"); len(apiUrl) >= 2 {
		endConfiguration.Host = apiUrl[2]
		if apiPort := strings.Split(endConfiguration.Host, ":"); len(apiPort) >= 2 {
			endConfiguration.Port = ":" + apiPort[1]
			endConfiguration.Host = apiPort[0]
		}
	}
	return endConfiguration
}

func (configurator *Configurator) GetUser(userFlag string, endConfiguration Configuration, env Configuration, conf Configuration, passwordFlag string) Configuration {
	if userFlag != "" {
		configurator.Logger.Info("Took user from parameter")
		endConfiguration.User = userFlag
	} else if env.User != "" {
		configurator.Logger.Info("Took User from env variable")
		endConfiguration.User = env.User
	} else if conf.User != "" {
		configurator.Logger.Info("Took User from configuration file")
		endConfiguration.User = conf.User
	}
	if passwordFlag != "" {
		configurator.Logger.Info("Took password from parameter")
		endConfiguration.Password = passwordFlag
	} else if env.Password != "" {
		configurator.Logger.Info("Took Password from env variable")
		endConfiguration.Password = env.Password
	} else if conf.Password != "" {
		configurator.Logger.Info("Took Password from configuration file")
		endConfiguration.Password = conf.Password
	}
	if env.PassCmd != "" {
		endConfiguration.PassCmd = conf.PassCmd
	} else if conf.PassCmd != "" {
		endConfiguration.PassCmd = conf.PassCmd
	}
	return endConfiguration
}

func (configurator *Configurator) RunPassCMD(endConfiguration Configuration) Configuration {
	if endConfiguration.PassCmd != "" {
		configurator.Logger.Info("Running passcmd command '" + endConfiguration.PassCmd + "' to retrieve password")
		out, err := exec.Command("sh", "-c", endConfiguration.PassCmd).Output()
		configurator.Logger.CheckCritical(err, ExitConfig)
		pass := strings.TrimSuffix(string(out), "\n")
		endConfiguration.Password = stringutil.StrToBase64(pass)
	}
	return endConfiguration
}

func (configurator *Configurator) GetAuthor(authorFlag string, endConfiguration Configuration, env Configuration, conf Configuration) Configuration {
	// set author information
	if authorFlag != "" {
		configurator.Logger.Info("Took author information from parameters")
		endConfiguration.Author = authorFlag
	} else if env.Author != "" {
		configurator.Logger.Info("Took author information from env variable")
		endConfiguration.Author = env.Author
	} else if conf.Author != "" {
		configurator.Logger.Info("Took author information from configuration file")
		endConfiguration.Author = conf.Author
	}
	if endConfiguration.Author == "" {
		endConfiguration.Author = endConfiguration.User
		configurator.Logger.Info("Author not set, fallback to username")
	}
	return endConfiguration
}

func (configurator *Configurator) GetApi(apiFlag string, env, conf, endConfiguration Configuration) Configuration {
	if apiFlag != "" {
		configurator.Logger.Info("Took api url from parameters")
		endConfiguration.Api = apiFlag
	} else if env.Api != "" {
		configurator.Logger.Info("Took api url from env variable")
		endConfiguration.Api = env.Api
	} else if conf.Api != "" {
		configurator.Logger.Info("Took api url from configuration file")
		endConfiguration.Api = conf.Api
	}
	return endConfiguration
}

func (configurator *Configurator) GetEnvVariables() Configuration {
	env := Configuration{os.Getenv("ICINGA_USER"), os.Getenv("ICINGA_PASS"), os.Getenv("ICINGA_API"), os.Getenv("ICINGA_AUTHOR"), os.Getenv("ICINGA_PASSCMD"), "", "", "", "Environment Variables"}
	return env
}
func (configurator *Configurator) GetConfVariables() Configuration {
	conf := Configuration{}
	usr, err := user.Current()
	configurator.Logger.CheckWarning(err)
	configFilePath := usr.HomeDir + "/.gocinga"
	configFile := ""
	for _, yamlExtension := range []string{"yaml", "yml"} {
		if _, err := os.Stat(configFilePath + "." + yamlExtension); err == nil {
			configFile = configFilePath + "." + yamlExtension
			configurator.Logger.Info("found", configFile)
		} else {
			configurator.Logger.CheckWarning(err)
		}
	}
	configurator.Logger.Info("using", configFile)
	data, err := ioutil.ReadFile(configFile)
	configurator.Logger.CheckWarning(err)
	err = yaml.Unmarshal([]byte(data), &conf)
	configurator.Logger.CheckWarning(err)
	conf.Name = "User Configuration"
	return conf
}
func (configurator *Configurator) BuildFromFile(filePath string) Configuration {
	conf := Configuration{}
	if _, err := os.Stat(filePath); err == nil {
		configurator.Logger.Info("found", filePath)
	} else {
		configurator.Logger.CheckWarning(err)
	}
	data, err := ioutil.ReadFile(filePath)
	configurator.Logger.CheckWarning(err)
	err = yaml.Unmarshal([]byte(data), &conf)
	configurator.Logger.CheckWarning(err)
	conf.Name = "Given file"
	return conf
}
func (configurator *Configurator) GetConfig() Configuration {
	return configurator.Config
}
