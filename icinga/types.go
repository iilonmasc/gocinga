package icinga

import (
	"net/http"
	"time"

	"gitlab.com/sambadevi/sambalib/v3/logutil"
)

type Endpoint struct {
	Name string
	Path string
}

type Api struct {
	Endpoints struct {
		Downtimes        Endpoint
		Hosts            Endpoint
		Services         Endpoint
		DowntimeSchedule Endpoint
		DowntimeRemove   Endpoint
	}
	Url      string
	SkipTLS  bool
	DryRun   bool
	Prettify bool
	Auth     struct {
		User string
		Pass string
	}
	HTTPClient *http.Client
	Logger     *logutil.Logger
}

type Downtime struct {
	Begin    time.Time
	Duration time.Duration
	End      time.Time
}
type PayloadObject struct {
	Url     string
	Content map[string]string
	Vars    map[int]string
}
type Configurator struct {
	Logger *logutil.Logger
	Config Configuration
}
type PayloadBuilder struct {
	Logger *logutil.Logger
}
type Configuration struct {
	User     string `yaml:"user"`
	Password string `yaml:"password"`
	Api      string `yaml:"api"`
	Author   string `yaml:"author"`
	PassCmd  string `yaml:"passcmd"`
	Access   string
	Port     string
	Host     string
	Name     string
}

const (
	ExitOkay = int(iota)
	ExitError
	ExitParam
	ExitConfig
	ExitPayload
	ExitAuth
	ExitServer
)
