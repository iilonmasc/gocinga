package icinga

import (
	"bytes"
	"encoding/json"
	"os"
	"strconv"
	"strings"
	"text/template"
	"time"

	"gitlab.com/sambadevi/sambalib/v3/logutil"
	"gitlab.com/sambadevi/sambalib/v3/stringutil"
)

func (builder *PayloadBuilder) Init() {
	builder.Logger = &logutil.Logger{}
	builder.Logger.SetOptions(logutil.LogLevel(logutil.Critical), logutil.Prefix("payload"))
}
func (builder *PayloadBuilder) EnableVerbosity() {
	builder.Logger.SetOptions(logutil.LogLevel(logutil.All))
}

func (builder *PayloadBuilder) BuildDowntime(duration, start string) Downtime {
	downtimeConfiguration := Downtime{}
	zone, _ := time.Now().Zone()
	var err error
	if start == "" {
		downtimeConfiguration.Begin = time.Now()
	} else {
		downtimeConfiguration.Begin, err = time.Parse(time.RFC822, start+" "+zone)
		builder.Logger.CheckWarning(err)
	}
	if duration == "" {
		duration = "1m"
	}
	milliseconds, err := stringutil.ConvertTime(duration)
	builder.Logger.CheckCritical(err, ExitPayload)
	if milliseconds == 0 {
		builder.Logger.Critical("duration is 0ms, no object will be created")
		os.Exit(ExitPayload)
	}
	duration = strconv.Itoa(milliseconds) + "ms"
	downtimeConfiguration.Duration, err = time.ParseDuration(duration)
	builder.Logger.CheckCritical(err, ExitPayload)
	downtimeConfiguration.End = downtimeConfiguration.Begin.Add(downtimeConfiguration.Duration)
	return downtimeConfiguration
}

func (builder *PayloadBuilder) CreateDowntimePayload(message, author string, downConfig Downtime) (string, error) {
	payload := PayloadObject{Url: "", Content: make(map[string]string), Vars: make(map[int]string)}
	payload.Content["start"] = strconv.FormatInt(downConfig.Begin.Unix(), 10)
	payload.Content["end"] = strconv.FormatInt(downConfig.End.Unix(), 10)
	payload.Content["duration"] = strconv.FormatInt(int64(downConfig.Duration.Seconds()), 10)
	if message == "" {
		message = "gocinga: downtime created by user " + author
	}
	payload.Content["message"] = stringutil.SurroundString(message, '"')
	payload.Content["author"] = stringutil.SurroundString(author, '"')
	payloadString := `{"start_time":{{ .Content.start }},"end_time":{{ .Content.end }},"duration":{{ .Content.duration }},"author":{{ .Content.author }},"comment":{{.Content.message}}}`
	tmpl, err := template.New("payload").Parse(payloadString)
	builder.Logger.CheckWarning(err)
	buffer := new(bytes.Buffer)
	err = tmpl.Execute(buffer, payload)
	return buffer.String(), err
}
func (builder *PayloadBuilder) CreateHostPayload(server string, vars []string) (string, error) {
	payload := PayloadObject{Url: "", Content: make(map[string]string), Vars: make(map[int]string)}
	payload.Content["address"] = stringutil.SurroundString(server, '"')
	payload.Content["checkCommand"] = stringutil.SurroundString("http", '"')

	var key, value string
	for index, variable := range vars {
		key = strings.Split(variable, "=")[0]
		value = strings.Split(variable, "=")[1]

		payload.Vars[index] = stringutil.SurroundString(key, '"') + ":" + stringutil.SurroundString(value, '"')
		key = ""
		value = ""
	}
	payloadString := `{"attrs":{"address":{{ .Content.address }},"check_command":{{ .Content.checkCommand }}{{ range .Vars }},{{.}}{{ end }}}}`
	tmpl, err := template.New("payload").Parse(payloadString)
	builder.Logger.CheckWarning(err)
	buffer := new(bytes.Buffer)
	err = tmpl.Execute(buffer, payload)
	return buffer.String(), err
}

func (builder *PayloadBuilder) ValidatePayload(rawPayload string) error {
	var js map[string]interface{}
	err := json.Unmarshal([]byte(rawPayload), &js)
	return err
}
