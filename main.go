package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/sambadevi/getopt/v2"
	"gitlab.com/sambadevi/gocinga/v3/icinga"
	"gitlab.com/sambadevi/sambalib/v3/logutil"
	"gitlab.com/sambadevi/sambalib/v3/stringutil"
)

func main() {
	rawMode := false
	currentType := "host"
	var currentEndpoint icinga.Endpoint
	var rawString, payload string
	var err error
	mainLogger := &logutil.Logger{}
	mainLogger.SetOptions(logutil.LogLevel(logutil.Critical), logutil.Prefix("gocinga"))

	// commands
	addFlag := getopt.StringLong("add", 'a', "", "add entry")
	deleteFlag := getopt.StringLong("delete", 'd', "", "delete entry")
	listFlag := getopt.BoolLong("list", 'l', "list entries of given type (see -t / --type)")
	// parameters user, password, api
	apiFlag := getopt.StringLong("api", 0, "", "api to connect with")
	authorFlag := getopt.StringLong("author", 0, "", "author for downtimes")
	passwordFlag := getopt.StringLong("password", 'p', "", "password to authenticate with")
	userFlag := getopt.StringLong("user", 'u', "", "authenticate as user")
	fileFlag := getopt.StringLong("file", 'f', "", "configuration file to use")
	// parameters for entries
	durationFlag := getopt.StringLong("duration", 0, "", "duration of the scheduled downtime (accepted formats: ms, s, m, h, d. Example: 1d for one day, 22m for 22 minutes, 10ms for 10 milliseconds)")
	messageFlag := getopt.StringLong("message", 'm', "", "downtime message")
	startTimeFlag := getopt.StringLong("start", 0, "", "optional start time in DD Mon YY HH:MM format (time in 24 hour format. Example: 05 Jun 11 12:32)")
	typeFlag := getopt.StringLong("type", 't', "", "type of entry (host, downtime)")
	configFlag := getopt.BoolLong("show-config", 'c', "display current configuration", "display current configuration")
	saveFlag := getopt.BoolLong("save-config", 0, "save current configuration", "save current configuration")
	dryrunFlag := getopt.BoolLong("dry-run", 0, "dry run, display payload at the end", "dry run, display payload at the end")
	helpFlag := getopt.BoolLong("help", 'h', "display help", "display help")
	tlsFlag := getopt.BoolLong("skip-tls", 0, "", "skip tls validation")
	verboseFlag := getopt.BoolLong("verbose", 'v', "verbose mode", "verbose mode")
	varsFlag := getopt.ListLong("attrs", 0, "List of attributes to add")
	rawFlag := getopt.StringLong("raw", 'r', "", "raw json payload (enclose in single-quotes or escape inner double-quotes)")
	versionFlag := getopt.BoolLong("version", 0, "get version information")
	prettyFlag := getopt.BoolLong("pretty", 0, "prettify json output")
	getopt.Parse()

	if *helpFlag {
		getopt.Usage()
		os.Exit(icinga.ExitOkay)
	}
	if *versionFlag {
		showVersion()
	}
	if *verboseFlag {
		mainLogger.SetOptions(logutil.LogLevel(logutil.All))
	}
	configurator := getConfigurator(*apiFlag, *userFlag, *passwordFlag, *authorFlag, *fileFlag, *verboseFlag)
	exec := configurator.GetConfig()
	if *configFlag {
		configurator.ShowConfig()
	}
	if *saveFlag {
		configurator.SaveConfig()
	}
	if exec == (icinga.Configuration{}) || exec.Access == "" || exec.Api == "" || exec.Host == "" {
		mainLogger.Critical("No credentials or api found! Use 'gocinga <your parameters> -c' to inspect your current configuration")
		os.Exit(icinga.ExitConfig)
	}
	api := getApi(exec, *tlsFlag, *dryrunFlag, *verboseFlag, *prettyFlag)
	builder := getPayloadBuider(*verboseFlag)

	if *addFlag == "" && *deleteFlag == "" && !*listFlag {
		mainLogger.Critical("No action set, use -h or --help to list valid options")
		os.Exit(icinga.ExitParam)
	}
	if *typeFlag != "" {
		currentType = strings.ToLower(*typeFlag)
		mainLogger.Info("ObjectType " + currentType)
		switch currentType {
		case "downtimes", "downtime":
			currentEndpoint = api.Endpoints.Downtimes
		case "services", "service":
			currentEndpoint = api.Endpoints.Services
		case "host", "hosts":
			currentEndpoint = api.Endpoints.Hosts
		default:
			{
				mainLogger.Critical("No known object type set")
				os.Exit(icinga.ExitParam)
			}
		}
	} else {
		currentEndpoint = api.Endpoints.Hosts
	}
	if *listFlag {
		api.List(currentEndpoint)
		os.Exit(icinga.ExitOkay)
	}

	if *rawFlag != "" {
		rawString = *rawFlag
		mainLogger.Info("--raw/-r set Ignoring payload configurations")
		if rawString == "-" {
			mainLogger.Info("--raw = '-' Reading from stdin")
			scanner := bufio.NewScanner(os.Stdin)
			scanText := ""
			for scanner.Scan() {
				scanText += scanner.Text()
			}
			err = scanner.Err()
			mainLogger.CheckCritical(err, icinga.ExitPayload)
			rawString = scanText
		}
		err := builder.ValidatePayload(rawString)
		mainLogger.CheckCritical(err, icinga.ExitPayload)
		rawMode = true
	}

	if *addFlag != "" && currentType == "host" {
		if rawMode {
			payload = rawString
		} else {
			payload, err = builder.CreateHostPayload(*addFlag, *varsFlag)
			mainLogger.CheckCritical(err, icinga.ExitPayload)

			mainLogger.Info("Payload generated " + stringutil.SurroundString(payload, '\''))

		}
		api.Add(*addFlag, payload, currentEndpoint)
	} else if *addFlag != "" && currentType == "downtime" {
		if exec.Author == "" {
			mainLogger.Critical("Missing Parameter for DOWNTIME Object" + stringutil.SurroundString("--author", '\''))
			os.Exit(icinga.ExitParam)
		}
		if rawMode {
			payload = rawString
		} else {

			downtimeConfiguration := builder.BuildDowntime(*durationFlag, *startTimeFlag)

			mainLogger.Info("Downtime Begin" + downtimeConfiguration.Begin.Format(time.RFC822))
			mainLogger.Info("Downtime End" + downtimeConfiguration.End.Format(time.RFC822))

			payload, err = builder.CreateDowntimePayload(*messageFlag, exec.Author, downtimeConfiguration)
			mainLogger.CheckCritical(err, icinga.ExitPayload)
		}
		api.Add(*addFlag, payload, currentEndpoint)
	} else if *deleteFlag != "" {
		api.Delete(*deleteFlag, "", currentEndpoint)
	} else {
		mainLogger.Critical("Missing values")
		os.Exit(icinga.ExitParam)
	}
}
func showVersion() {
	currentVersion := "v3.1.0"
	fmt.Printf("gocinga %s\n", currentVersion)
	os.Exit(icinga.ExitOkay)
}
func getApi(exec icinga.Configuration, skipTls, dryrun, verbose, pretty bool) *icinga.Api {
	api := &icinga.Api{}
	api.Init(exec.Api)
	if skipTls {
		api.DisableTLS()
	}
	if dryrun {
		api.EnableDryRun()
	}
	if pretty {
		api.EnablePrettify()
	}
	if verbose {
		api.EnableVerbosity()
	}
	api.SetAccess(exec.User, exec.Password)
	api.CreateClient()
	return api
}
func getConfigurator(apiFlag, userFlag, passwordFlag, authorFlag, fileFlag string, verbose bool) *icinga.Configurator {
	configurator := &icinga.Configurator{}
	configurator.Init()
	if verbose {
		configurator.EnableVerbosity()
	}
	configurator.BuildConfig(apiFlag, userFlag, passwordFlag, authorFlag, fileFlag)
	return configurator
}
func getPayloadBuider(verbose bool) *icinga.PayloadBuilder {
	builder := &icinga.PayloadBuilder{}
	builder.Init()
	if verbose {
		builder.EnableVerbosity()
	}
	return builder
}
