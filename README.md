# gocinga: CLI Tool for Icinga, written in go

## Installation
First you need to install `golang` for your system, the instructions for your system can be found [on the golang.org Getting Started page](https://golang.org/doc/install)

After you installed `go`, verify that your installation is working:

    $ go version
    go version go1.12 darwin/amd64

Now simply run `go get` to install the tool to your system:

    $ GO111MODULE=on go get -u gitlab.com/sambadevi/gocinga/v3

You need to have `$GOPATH` in your `$PATH` to be able to access the binaries in `$GOPATH/bin`, you might want to add this line to your `.zshrc` (syntax may differ for `.bashrc`)

    $ export PATH=${GOPATH//://bin:}/bin:$PATH

If calling `gocinga` results in a `command not found` error you need to reload/restart your shell.

## Configuration

There are three ways to configure this tool, ordered by priority:
1. CLI Parameters
2. Environment Variables
3. Configuration file `~/.gocinga.yml` or `~/.gocinga.yaml` (`.yml` will be preferred if both exist)

CLI-Parameters will overwrite Environment Variables, Environment Variables will overwrite the Configuration file

Your final configuration will be a combination from the given parameters, your environment variables and the configuration file.

Example:
    
    # Set ICINGA_API environment variable
    $ export ICINGA_API="https:/myicinga.example.com/v1"

    # Set "access" in configuration file
    $ echo 'user: myUser' > ~/.gocinga.yml
    $ echo 'password: myBase64Password' >> ~/.gocinga.yml

    # Show current configuration of gocinga
    $ gocinga -c
    Access-Token:     myUser:myBase64Password                # <- This is from the config file
    Icinga-User:      myUser
    Downtime-Author:  myUser                           # <- defaults to icinga-username
    Icinga-Password:  myBase64Password
    Icinga-API:       https://myicinga.example.com  # <- This is from the env variable
    Icinga-Host:      myicinga.example.com:5665

    # Show current configuration but set user to myOtherUser
    $ gocinga -c -u myOtherUser
    Access-Token:     myOtherUser:myBase64Password                # <- This is from the config file
    Icinga-User:      myOtherUser
    Downtime-Author:  myUser                           # <- defaults to icinga-username
    Icinga-Password:  myBase64Password
    Icinga-API:       https://myicinga.example.com  # <- This is from the env variable
    Icinga-Host:      myicinga.example.com:5665


### via Parameters
Simply use the `--user/-u`, `--password/-p` and `--api` flags to setup your environment

### via Environment variables
You can set the `ICINGA_USER`, `ICINGA_PASS`, `ICINGA_API` and `ICINGA_AUTHOR` variables to the corresponding values.

`ICINGA_USER` is your user and `ICINGA_PASS` the base64 encoded password.
`ICINGA_API` is the url to your Icinga API, **including** the API version (`/v1` in this example)
`ICINGA_AUTHOR` is the name which will be added to scheduled downtimes as author

    $ export ICINGA_USER="myUser"
    $ export ICINGA_PASS="myBase64Password"
    $ export ICINGA_API="https://myicinga.example.com"
    $ export ICINGA_AUTHOR="Dave Developer"

### via Configuration file
You can also set your `~/.gocinga.yml` configuration in YAML format, the same rules apply as for the environment variables:

    # ~/.gocinga.yml
    user: myUser
    pass: myBase64Password
    api: https://myicinga.example.com
    author: Dave Developer

#### passcmd

##### configuration

You can add the `passcmd` field to your `.gocinga.yml` configuration and specify a command to be run to get your (plaintext) password. For example using `pass`:

    # ~/.gocinga.yml
    user: myUser
    passcmd: pass path/to/my/api/password
    api: https://myicinga.example.com
    author: Dave Developer

##### env variable

`ICINGA_PASSCMD` is the command gocinga will run to retrieve your password instead of using something else. (Your password command should return the password in plain-text)


    $ export ICINGA_PASSCMD="pass path/to/my/api/password"

This will run `pass path/to/my/api/password` to retrieve your password and use it to authenticate against your icinga api.

If this command is not found or encounters an error, gocinga will stop execution, in this case you should add the `-v` flag to your command to examine the problem with your passcmd.

#### From custom location

You can use `-f`/`--file` and point gocinga to a configuration file of your choice. This will prevent gocinga completely to read any configuration it finds and only use the values provided by the configuration file.

    $ gocinga -c -f /tmp/guest.yaml
    Access-Token:     guest:guestPass
    Icinga-User:      guest
    Downtime-Author:  guest account
    Icinga-Password:  guestPass
    Icinga-API:       https://different.icinga.example.com
    Icinga-Host:      different.icinga.example.com:9001

### From command
`gocinga --save-config` will save your current config at `$HOME/.gocinga.yml` you **can** use the `-p, -u, --api, --author` parameters to specify the config fields.

    # using the last configuration from "via Environment variables" but overriding myUser with apiUser and changing author as well
    $ gocinga --save-config -u apiUser --author "Generic API User"
    $ cat ~/.gocinga.yml
    # config file created with gocinga --save-config
    user: apiUser
    pass: myBase64Password
    api: https://myicinga.example.com
    author: Generic API User

## Usage
### Parameters
#### Actions
|Option|Example|Description|
|------|-------|-----------|
|`--add`, `-a`|`--add myserver.com`|Add a new entry to your configured Icinga System|
|`--delete`, `-d`|`--delete myserver.com`|Remove an entry from your configured Icinga System|
|`--list`, `-l`||List entries of the given type (see -t / --type)|

#### General Configuration
|Option|Example|Description|
|------|-------|-----------|
|`--api`| `--api https://myicinga.example.com` | The api to connect with |
|`--author`| `--author "Dave Developer"` | Author for scheduled downtimes |
|`--password`, `-p`| `--password myBase64Password` | The base64 encoded password to authenticate with |
|`--user`, `-u`| `--user myUser` | The user to authenticate as |
|`--file`, `-f`| `--file /my/custom/gocingauration.yaml` | Custom configuration file to use (overrides everything) |

#### Entry Configuration
|Option|Example|Description|
|------|-------|-----------|
|`--duration`| `--duration 10m` | Downtime duration (accepted formats: ms, s, m, h, d. Example: 1d for one day, 22m for 22 minutes, 10ms for 10 milliseconds), this defaults to `1m`|
|`--message`, `-m`| `--message "Fixing stuff"` | Add a message as downtime-comment, defaults to "gocinga: downtime created by user '[your-user]'"|
|`--attrs`| `--attrs "vars.type=webserver"` | Add variables to the json payload (example = `"vars.type": "webserver"`)|
|`--raw`, `-r`| `-r '{...some valid json...}'` | use a raw json payload instead of parameters. If you specify `-` then gocinga will read from *stdin* instead |
|`--start`| `--start "05 Jun 11 12:32"` | optional start time in DD Mon YY HH:MM format (time in 24 hour format.)|
|`--type`| `--type downtime` | Type of entry to use (host, downtime)|

#### Miscellaneous Options
|Option|Description|
|------|-----------|
|`--show-config`, `-c`|Displays the current configuration, combined from parameters, environment and user configuration|
|`--save-config`|Save the currently set configuration to `~/.gocinga.yml`, accepts the same parameters as any command|
|`--dry-run`|Prints the resulting URL and Payload but does not send any data to the server|
|`--help`, `-h`|Displays the Usage information|
|`--pretty`|Prettify JSON Output from Icinga API|
|`--skip-tls`|Skips SSL Certificate verification (if you are using self-signed certificates or your certificate expired)|
|`--verbose`, `-v`| Verbose mode, displays additional INFO and WARN messages|
|`--version`| Displays the current version of gocinga|


### Examples

    # Add an entry with vars.os "webserver" for otherserver.example.com
    $ gocinga -a otherserver.example.com --attrs "vars.os=webserver" -v
    # ... some verbose lines inbetween ...
    # The response should be
    2019/03/18 23:59:05 [INFO] Response: {"results":[{"code":200.0,"status":"Object was created"}]}

To delete an entry you just need to specifiy the name of the server which you would like to remove

    # Delete an entry
    $ gocinga -d deleteme.example.com -v
    # ... some verbose lines inbetween ...
    # The response should be
    2019/03/19 00:02:51 [INFO] Response: {"results":[{"code":200.0,"errors":[],"name":"deleteme.example.com","status":"Object was deleted.","type":"Host"}]}

Add a downtime for any given host and any service registered on the host (host.name == downtime.example.com)

    # Add a 20 minute downtime for downtime.example.com
    $ gocinga -a downtime.example.com --type downtime --duration "20m" -v
    ... a lot of lines with (hopefully) status: successfully scheduled ...

Delete any downtimes registered for a given host

    # Remove downtimes for downtime.example.com
    $ gocinga -d downtime.example.com --type downtime -v
    # ... some verbose lines inbetween ...
    {"results":[{"code":200.0,"status":"Successfully removed all downtimes for object 'downtime.example.com'."}]}

### Custom Variables

The `--attrs` flag accepts any combination of `key=value` and adds a custom field to the json payload:

    # Add an entry with multiple custom fields
    $ gocinga -a customfields.example.com --attrs "vars.timeout=200" --attrs "vars.os=Linux" --attrs "vars.location=EU" --attrs "vars.uri=/access"

This will create a new host object with the additional fields:
- **vars.timeout** with value **200**
- **vars.os** with value **Linux**
- **vars.location** with value **EU**
- **vars.uri** with value **/access**

### Raw payloads

The `--raw` and `-r` flags can be used to send a raw json payload.

    $ gocinga -a raw.example.com --skip-tls -v -r '{"attrs": {"address": "raw.example.com","check_command": "http","vars.foo": "bar","vars.os": "linux"}}'
    
> **Note:** You still need to add the servername to the command flags (`-a`, `-d`). Otherwise the tool does not know which url it should call.

#### Read from stdin

`--raw` can read from stdin if you specify `-` as value:

    $ gocinga -a stdin.example.com --skip-tls -v -r - < myPayload.json
    $ myComplexCommandChainReturningJSON | gocinga -a stdin.example.com --skip-tls -v -r -
